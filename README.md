# Word counter

## Build

Using gradle to build:  gradlew clean build

## Run

Using bootrun locally:  gradlew bootRun

##  Swagger

```
GET http://localhost:8081/wordcounter/api-docs
```

### Swagger UI
```
http://localhost:8081/wordcounter/swagger-ui/index.html
```

## Rest implemented

● CalculateHighestFrequency should return the highest frequency in the text (several
words might have this frequency)
```
POST http://localhost:8081/wordcounter/calculateHighestFrequency
Accept: */*
Content-Type: application/json

{
"text" :"The sun shines over the lake"
}
```

● CalculateFrequencyForWord should return the frequency of the specified word
```
POST http://localhost:8081/wordcounter/calculateFrequencyForWord
Accept: */*
Content-Type: application/json

{
"text" :"",
"word" : "the"
}
```

● CalculateMostFrequentNWords should return a list of the most frequent „n‟ words in
the input text, all the words returned in lower case. If several words have the same
frequency, this method should return them in ascendant alphabetical order (for input
text “The sun shines over the lake” and n = 3, it should return the list {(“the”, 2),
(“lake”, 1), (“over”, 1) }
```
POST http://localhost:8081/wordcounter/calculateMostFrequentNWords
Accept: */*
Content-Type: application/json

{
  "text" :"The sun shines over the lake",
  "n": 2
}
```
