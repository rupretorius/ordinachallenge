package com.ordina.sample.wordcounter.api.representation;

public class WordFrequencyRequest {
  private String text;
  private Integer n;
  private String word;

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public Integer getN() {
    return n;
  }

  public void setN(Integer n) {
    this.n = n;
  }

  public String getWord() {
    return word;
  }

  public void setWord(String word) {
    this.word = word;
  }
}
