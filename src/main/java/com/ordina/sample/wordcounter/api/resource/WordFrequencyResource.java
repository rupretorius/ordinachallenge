package com.ordina.sample.wordcounter.api.resource;

import com.ordina.sample.wordcounter.api.representation.WordFrequencyRequest;
import com.ordina.sample.wordcounter.service.WordFrequency;
import com.ordina.sample.wordcounter.service.WordFrequencyAnalyzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class WordFrequencyResource {

  @Autowired
  private WordFrequencyAnalyzer wordFrequencyAnalyzer;

  @PostMapping(value = "/calculateFrequencyForWord", consumes = "application/json")
  int calculateFrequencyForWord(@RequestBody WordFrequencyRequest request) {
    if (request == null || !StringUtils.hasLength(request.getText()) || !StringUtils.hasLength(request.getWord())) {
      throw new ResponseStatusException(
              HttpStatus.BAD_REQUEST, "Request must have text and word");
    }
    return wordFrequencyAnalyzer.calculateFrequencyForWord(request.getText(), request.getWord());
  }

  @PostMapping(value = "/calculateHighestFrequency", consumes = "application/json")
  int calculateHighestFrequency(@RequestBody WordFrequencyRequest request) {
    if (request == null || !StringUtils.hasLength(request.getText())) {
      throw new ResponseStatusException(
              HttpStatus.BAD_REQUEST, "Request must have text");
    }
    return wordFrequencyAnalyzer.calculateHighestFrequency(request.getText());
  }

  @PostMapping(value = "/calculateMostFrequentNWords", consumes = "application/json")
  List<WordFrequency> calculateMostFrequentNWords(@RequestBody WordFrequencyRequest request) {
    if (request == null || !StringUtils.hasLength(request.getText()) || request.getN() == null ) {
      throw new ResponseStatusException(
              HttpStatus.BAD_REQUEST, "Request must have text and n");
    }
    return wordFrequencyAnalyzer.calculateMostFrequentNWords(request.getText(), request.getN());
  }


}
