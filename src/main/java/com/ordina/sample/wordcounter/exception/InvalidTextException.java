package com.ordina.sample.wordcounter.exception;

public class InvalidTextException extends Exception {
  public InvalidTextException(String message) {
    super(message);
  }
}
