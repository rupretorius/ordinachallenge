package com.ordina.sample.wordcounter.service;

public class WordFrequencyImpl implements WordFrequency {

  private String word;
  private int frequency;

  //Default constructor needed for json parsing
  public WordFrequencyImpl() {
    super();
  }

  public WordFrequencyImpl(String word, int frequency) {
    this.word = word;
    this.frequency = frequency;
  }

  @Override
  public String getWord() {
    return word;
  }

  @Override
  public int getFrequency() {
    return frequency;
  }
}
