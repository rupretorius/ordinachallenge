package com.ordina.sample.wordcounter.service;

public interface WordFrequency {
  String getWord();
  int getFrequency();
}
