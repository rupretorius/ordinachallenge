package com.ordina.sample.wordcounter.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class WordFrequencyAnalyzerImpl implements WordFrequencyAnalyzer {

  private static final Logger log = LoggerFactory.getLogger(WordFrequencyAnalyzerImpl.class);

  @Override
  public int calculateHighestFrequency(String text) {
    log.debug("CalculateHighestFrequency");
    validateText(text);
    Map<String, Integer> wordCount = parseText(text);
    Optional<Map.Entry<String, Integer>> opt = wordCount.entrySet().stream().max(Map.Entry.comparingByValue());
    if (opt.isPresent()) {
      return opt.get().getValue();
    } else {
      return 0;
    }
  }

  @Override
  public int calculateFrequencyForWord(String text, String word) {
    log.debug("CalculateFrequencyForWord");
    validateText(text);
    Map<String, Integer> wordCount = parseText(text);
    return wordCount.getOrDefault(word.toLowerCase(), 0);
  }

  @Override
  public List<WordFrequency> calculateMostFrequentNWords(String text, int n) {
    log.debug("CalculateMostFrequentNWords");
    validateText(text);
    Map<String, Integer> wordCount = parseText(text);
    List<WordFrequency> tempList = new ArrayList<>();
    //Sort map by the count descending and word ascending
    Map<String, Integer> sorted = wordCount.entrySet().stream()
            .sorted(Map.Entry.<String, Integer>comparingByValue().reversed()
                    .thenComparing(Map.Entry.comparingByKey()))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (v1, v2) -> v1, LinkedHashMap::new));
    //Add items to list
    sorted.forEach((key, value) -> tempList.add(new WordFrequencyImpl(key, value)));
    //Limit list according to n
    return tempList.stream().limit(n).collect(Collectors.toList());
  }

  private Map<String, Integer> parseText(String text) {
    //Replace all non alpha numeric characters with #
    String s = text.replaceAll("[^A-Za-z]", "#");
    //Remove sequence of #.  Only one separator per word
    s = s.replaceAll("(#)\\1", "#");
    //Split string with space separator
    String[] splits = s.split("#");
    //Add words to map with counter
    Map<String, Integer> wordCount = new HashMap<>();
    for (String split : splits) {
      String key = split.toLowerCase();
      Integer count = wordCount.get(key);
      if (count == null) {
        count = 0;
      }
      wordCount.put(key, count + 1);
    }
    return wordCount;
  }

  private void validateText(String text) {
    if (!StringUtils.hasLength(text)) {
      throw new RuntimeException("Text cannot be null or blank");
    }
  }
}
