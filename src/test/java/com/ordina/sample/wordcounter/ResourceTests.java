package com.ordina.sample.wordcounter;

import com.ordina.sample.wordcounter.api.representation.WordFrequencyRequest;
import com.ordina.sample.wordcounter.service.WordFrequencyImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ResourceTests {
  @Autowired
  private TestRestTemplate restTemplate;

  @Test
  public void testCalculateFrequencyForWord() {
    WordFrequencyRequest request = new WordFrequencyRequest();
    request.setWord("the");
    request.setText("The sun shines over the lake");

    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    headers.setContentType(MediaType.APPLICATION_JSON);
    HttpEntity<WordFrequencyRequest> entity = new HttpEntity<>(request, headers);

    ResponseEntity<Integer> response = this.restTemplate.exchange("/calculateFrequencyForWord",
            HttpMethod.POST, entity, Integer.class);
    Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    Assert.assertEquals(2, response.getBody().intValue());
  }

  @Test
  public void testCalculateHighestFrequency() {
    WordFrequencyRequest request = new WordFrequencyRequest();
    request.setText("The sun shines over the lake");

    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    headers.setContentType(MediaType.APPLICATION_JSON);
    HttpEntity<WordFrequencyRequest> entity = new HttpEntity<>(request, headers);

    ResponseEntity<Integer> response = this.restTemplate.exchange("/calculateHighestFrequency",
            HttpMethod.POST, entity, Integer.class);
    Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    Assert.assertEquals(2, response.getBody().intValue());
  }

  @Test
  public void testCalculateMostFrequentNWords() {
    WordFrequencyRequest request = new WordFrequencyRequest();
    request.setText("The sun shines over the lake");
    request.setN(3);

    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    headers.setContentType(MediaType.APPLICATION_JSON);
    HttpEntity<WordFrequencyRequest> entity = new HttpEntity<>(request, headers);

    ResponseEntity<List<WordFrequencyImpl>> response = this.restTemplate.exchange("/calculateMostFrequentNWords",
            HttpMethod.POST, entity, new ParameterizedTypeReference<List<WordFrequencyImpl>>() {
            });
    Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    Assert.assertEquals(3, response.getBody().size());
  }
}
