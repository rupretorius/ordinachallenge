package com.ordina.sample.wordcounter;

import com.ordina.sample.wordcounter.service.WordFrequency;
import com.ordina.sample.wordcounter.service.WordFrequencyAnalyzer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WordCounterApplicationTests {

  private static final Logger log = LoggerFactory.getLogger(WordCounterApplicationTests.class);
  @Autowired
  private WordFrequencyAnalyzer wordFrequencyAnalyzer;

  @Test(expected = RuntimeException.class)
  public void testNullText() {
    wordFrequencyAnalyzer.calculateHighestFrequency(null);
    Assert.fail();
  }

  @Test(expected = RuntimeException.class)
  public void testEmptyText() {
    wordFrequencyAnalyzer.calculateHighestFrequency("");
    Assert.fail();
  }

  @Test
  public void testFrequencyForWord() {
    int freq = wordFrequencyAnalyzer.calculateFrequencyForWord("The sun shines over the lake", "the");
    Assert.assertEquals(2, freq);
    freq = wordFrequencyAnalyzer.calculateFrequencyForWord("The sun shines over the lake", "moon");
    Assert.assertEquals(0, freq);
    freq = wordFrequencyAnalyzer.calculateFrequencyForWord("The-sun-shines-over-the-lake-and-the-ground", "the");
    Assert.assertEquals(3, freq);
    freq = wordFrequencyAnalyzer.calculateFrequencyForWord("The%sun-shines%over%the#lake@and(the`ground", "the");
    Assert.assertEquals(3, freq);
  }

  @Test
  public void testHighestFrequency() {
    int freq = wordFrequencyAnalyzer.calculateHighestFrequency("The sun shines over the lake");
    Assert.assertEquals(2, freq);

    freq = wordFrequencyAnalyzer.calculateHighestFrequency("The sun shines over the lake sun");
    Assert.assertEquals(2, freq);
  }

  @Test
  public void testMostFrequentNWords() {
    List<WordFrequency> frequencies = wordFrequencyAnalyzer.calculateMostFrequentNWords("The sun shines over the lake sun", 2);
    Assert.assertEquals(2, frequencies.size());
    Assert.assertEquals("sun", frequencies.get(0).getWord());
    Assert.assertEquals(2, frequencies.get(0).getFrequency());

    frequencies = wordFrequencyAnalyzer.calculateMostFrequentNWords("The sun shines over the lake", 3);
    Assert.assertEquals(3, frequencies.size());
    Assert.assertEquals("the", frequencies.get(0).getWord());
    Assert.assertEquals(2, frequencies.get(0).getFrequency());

    frequencies = wordFrequencyAnalyzer.calculateMostFrequentNWords("The sun shines over the lake", 1);
    Assert.assertEquals(1, frequencies.size());
    Assert.assertEquals("the", frequencies.get(0).getWord());
    Assert.assertEquals(2, frequencies.get(0).getFrequency());

    frequencies = wordFrequencyAnalyzer.calculateMostFrequentNWords("The sun shines over the lake", 0);
    Assert.assertEquals(0, frequencies.size());
    log.debug("Tested calculateMostFrequentNWords");


  }

  @Test
  public void testReadFromText() throws IOException {
    File resource = new ClassPathResource("sampleText.txt").getFile();
    String text = new String(Files.readAllBytes(resource.toPath()));
    int freq = wordFrequencyAnalyzer.calculateHighestFrequency(text);
    System.out.println("Highest frequency=" + freq);
    List<WordFrequency> frequencies = wordFrequencyAnalyzer.calculateMostFrequentNWords(text, 5);
    System.out.println("Top 5 word frequencies");
    for (WordFrequency frequency : frequencies) {
      System.out.println(frequency.getWord() + "   " + frequency.getFrequency());
    }
  }

  @Test
  public void testReadFromTextBig() throws IOException {
    File resource = new ClassPathResource("sampleText.txt").getFile();
    String text = new String(Files.readAllBytes(resource.toPath()));
    for (int i = 0; i < 10; i++) {
      text = text + " " + text;
    }
    List<WordFrequency> frequencies = wordFrequencyAnalyzer.calculateMostFrequentNWords(text, 5);
    System.out.println("Top 5 word frequencies");
    for (WordFrequency frequency : frequencies) {
      System.out.println(frequency.getWord() + "   " + frequency.getFrequency());
    }
  }

}
